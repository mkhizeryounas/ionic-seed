import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage';
import { HTTP } from '@ionic-native/http';
import { MenuController } from 'ionic-angular';


import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email:string;
  password:string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private nativeStorage: NativeStorage,
    private http: HTTP,
    private menu: MenuController
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    console.log(this.navParams.get('name'));
    this.menu.enable(false);
    this.navCtrl.swipeBackEnabled=false;
  }

  gotoSignup() {
    // this.navCtrl.push(SignupPage);
  }
  login() {
    let toast = this.toastCtrl.create({
      message: 'Invalid email / password',
      duration: 3000,
      position: 'bottom'
    });
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    if(this.email && this.password) {
      loading.present();

      setTimeout(() => {
        this.nativeStorage.setItem('userData', {
          name: 'Khizer Younas', 
          email: 'm.khizeryounas@ucp.edu.pk',
          id: 1,
        }).then(() => {
            loading.dismiss();
            
            this.navCtrl.setRoot(HomePage);

            console.log('Stored item!')
          },(error) => {
            console.error('Error storing item', error)
          }
        );
        
      }, 500);
    }
    else {
      toast.present();
    }
    console.log(this.email);
  }
}
