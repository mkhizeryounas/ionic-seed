import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';

import { NativeStorage } from '@ionic-native/native-storage';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { MenuController } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    private nativeStorage: NativeStorage,
    private toastCtrl: ToastController,
    public menuCtrl: MenuController
  ) {

  }
  ionViewDidLoad() {
    this.checkLoginState();
    this.navCtrl.swipeBackEnabled=false;
    this.menuCtrl.enable(true);
  }
  checkLoginState() {
    this.nativeStorage.getItem('userData').then((res)=>{
      console.log('Logged in');
    },(err)=>{
      this.navCtrl.setRoot(LoginPage);
    })
  }
  search(e) {
    console.log(e);
  }
  goto() {
    console.log('clicked')
    this.navCtrl.push(LoginPage,{
      name: "Khizer"
    });
  }
  openMenu() {
    this.menuCtrl.open();
  }
  

}
