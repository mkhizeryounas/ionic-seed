import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';

import { Toast } from 'ionic-angular/components/toast/toast';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { App } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

import { ActionSheetController } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private toastCtrl: ToastController,
    private nativeStorage: NativeStorage,
    private app: App,
    private menuCtrl: MenuController,
    private actionSheetCtrl: ActionSheetController
  ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  logout() {
    let toast = this.toastCtrl.create({
      message: 'Logged out successfuly',
      duration: 3000,
      position: 'bottom'
    });
    this.nativeStorage.remove('userData').then(()=>{
      toast.present();
      this.checkLoginState();
    }, () => {
      console.log('Error occoured');
    })
  }
  checkLoginState() {
    this.nativeStorage.getItem('userData').then((res)=>{
      console.log('Logged in');
    },(err)=>{
      this.app.getActiveNav().setRoot(LoginPage);
    })
  }
  headerMore() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'More',
      buttons: [
        {
          text: 'Logout',
          role: 'destructive',
          handler: () => {
            this.logout();
          }
        }
      ]
    });
    actionSheet.present();
  }
  openPage(page) {
    this.menuCtrl.close();
    if(page == 'profile') {
      this.app.getActiveNav().push(ProfilePage);
    }
  }

}

